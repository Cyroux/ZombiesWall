﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombiesWall
{
    class Program
    {
        static void Main(string[] args)
        {
            // Init soldats
            List<Soldat> s = new List<Soldat>();
            for (int i = 0; i < 3; i++)
            {
                s.Add(new Soldat());
            }

            // Init murs
            List<Mur> m = new List<Mur>();
            for (int i = 0; i < 1; i++)
            {
                m.Add(new Mur());
            }

            // Round infinis
            while (s.Count != 0)
            {
                Round r = new Round(10);
                // Init zombis
                List<Zombie> z = new List<Zombie>();
                for (int i = 0; i < r.NbZombis; i++)
                {
                    z.Add(new Zombie());
                }

                // première phase : phase d'approche
                Console.WriteLine("------ PHASE D'APPROCHE ------");
                Console.ReadLine();
                // deuxieme phase : phase d'attaque
                do
                {
                    Console.WriteLine("------ LES SOLDATS ATTAQUENT ------");
                    Console.ReadLine();
                    // Les soldats attaquent en premier :
                    for (int i = 0; i < s.Count; i++)
                    {
                        if (z.Count > 0) s.ElementAt(i).attaquerZombies(z);
                    }
                    Console.WriteLine("------ FIN DU TOUR DES SOLDATS ------");
                    Console.ReadLine();
                    // Les zombies attaquent ensuite
                    // Ils attaquent les murs s'il en reste, sinon, ils attaquent les soldats
                    Console.WriteLine("------ DEBUT DU TOUR DES ZOMBIS ------");
                    Console.ReadLine();
                    for (int i = 0; i < z.Count; i++)
                    {
                        if (m.Count > 0)
                        {
                            z.ElementAt(i).attaquerMurs(m);
                        }
                        else if (s.Count > 0)
                        {
                            z.ElementAt(i).attaquerSoldats(s);
                        }
                    }
                    Console.WriteLine("------ Fin d'un tour ------");
                    Console.WriteLine("Il reste " + z.Count + " zombis, " + s.Count + " soldats et " + m.Count + " murs.");
                    Console.ReadLine();
                } while (z.Count != 0 && s.Count > 0);
                if (s.Count == 0)
                {
                    Console.WriteLine("Il n'y a plus de soldat ! Fin de la partie");
                    Console.ReadLine();
                }
            }
        }
    }
}
