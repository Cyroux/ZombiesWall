﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombiesWall
{
    class Round
    {
        private int nbZombis;


        public Round(int nbZ)
        {
            nbZombis = nbZ;
        }
        public int NbZombis
        {
            get
            {
                return nbZombis;
            }

            set
            {
                nbZombis = value;
            }
        }
    }
}
