﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombiesWall
{
    class Soldat
    {
        private int niveau;     
        private int maxHP;
        private int hp;
        private int nbKill;
        private int degats;
        static Random rnd = new Random();
        public Soldat()
        {
            Niveau = 1;
            MaxHP = 3 * Niveau;
            hp = MaxHP;
            NbKill = 1;
            degats = 1;
        }

        public int Niveau
        {
            get
            {
                return niveau;
            }

            set
            {
                niveau = value;
            }
        }

        public int MaxHP
        {
            get
            {
                return maxHP;
            }

            set
            {
                maxHP = value;
            }
        }

        public int NbKill
        {
            get
            {
                return nbKill;
            }

            set
            {
                nbKill = value;
            }
        }

        public int Hp
        {
            get
            {
                return hp;
            }

            set
            {
                hp = value;
            }
        }

        public void attaquerZombies(List<Zombie> z)
        {
            for (int i = 0; i < nbKill; i++)
            {
                if (z.Count > 0)
                {
                    int index = rnd.Next(0, z.Count);
                    Console.WriteLine("Le soldat attaque le zombie numéro " + index);
                    z.ElementAt(index).Hp -= degats;

                    // Si le zombie est mort
                    if (z.ElementAt(index).Hp == 0)
                    {
                        // On fait XP le soldat
                        gagnerNiveau();
                        z.RemoveAt(index);
                        Console.WriteLine("Un soldat a tué un zombie !");
                    }
                }
            }
        }

        public void gagnerNiveau()
        {
            niveau++;
            nbKill = (niveau / 10) + 1;
            Console.WriteLine("Un soldat a gagné un niveau !");
        }
    }
}
