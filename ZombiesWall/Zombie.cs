﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZombiesWall
{
    class Zombie
    {
        private int degats;
        private int hp;
        static Random rnd = new Random();

        public int Degats
        {
            get
            {
                return degats;
            }

            set
            {
                degats = value;
            }
        }

        public int Hp
        {
            get
            {
                return hp;
            }

            set
            {
                hp = value;
            }
        }

        public Zombie()
        {
            degats = 1;
            hp = 1;
        }

        public void attaquerSoldats(List<Soldat> s)
        {
            int index = rnd.Next(0, s.Count);
            s.ElementAt(index).Hp -= degats;
            if (s.ElementAt(index).Hp == 0)
            {
                s.RemoveAt(index);
                Console.WriteLine("Un soldat a été tué par un zombie !");
            }
        }

        public void attaquerMurs(List<Mur> m) {

            Random rnd = new Random();
            int index = rnd.Next(0, m.Count);
            m.ElementAt(index).Hp -= degats;
            if(m.ElementAt(index).Hp == 0)
            {
                m.RemoveAt(index);
                Console.WriteLine("Un zombie a détruit un mur !");
            }
        }
    }
}
